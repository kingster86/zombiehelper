﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zombie
{
    class ExcelTransformer
    {

        public string InputFileDirectory { get; set; }

        private Application _app;

        public ExcelTransformer()
        {
            _app = new Application();
        }

        private IEnumerable<Address> handleInputFile(string inputFile)
        {
            var workBook = _app.Workbooks.Open(inputFile);
            var sheet = workBook.Sheets[1] as Worksheet;
            var range = sheet.UsedRange;
            var rowCount = range.Rows.Count;
            for (var r=1;r<=rowCount;r++)
            {
                var a = new Address() { FirstName = sheet.Cells[r, 1].Value, LastName = sheet.Cells[r, 2].Value };
                yield return a;
            }
            workBook.Close();
        }

        public void Transform()
        {
            var inputFiles = System.IO.Directory.GetFiles(InputFileDirectory);

            foreach (var inputFile in inputFiles)
            {
                try
                {
                    var addresses = handleInputFile(inputFile);
                    var list = new List<Address>(addresses);
                }
                catch
                {
                }
            }

        }
    }
}
